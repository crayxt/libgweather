# German libgweather translation
# Copyright (C) 1999-2004 Free Software Foundation, Inc.
# Carsten Schaar <nhadcasc@fs-maphy.uni-hannover.de>, 1998.
# Matthias Warkus <mawa@iname.com>, 1999, 2000.
# Karl Eichwalder <ke@suse.de>, 2000.
# Benedikt Roth <Benedikt.Roth@gmx.net>, 2000.
# Christian Neumair <chris@gnome-de.org>, 2002-2004.
# Hendrik Richter <hendrikr@gnome.org>, 2004, 2005, 2006, 2007, 2009.
# Hendrik Brandt <heb@gnome-de.org>, 2004-2005.
# Jochen Skulj <jochen@jochenskulj.de>, 2006.
# Andre Klapper <ak-47@gmx.net>, 2008.
# Christian Kirbach <Christian.Kirbach@googlemail.com>, 2009.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2009, 2012-2013, 2016.
# Wolfgang Stöggl <c72578@yahoo.de>, 2014.
# Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: libgweather master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=libgweather&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2016-12-22 16:00+0000\n"
"PO-Revision-Date: 2016-12-23 21:49+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: Deutsch <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.11\n"

#: ../data/glade/libgweather.xml.in.h:1
msgid "Location Entry"
msgstr "Ortseintrag"

#: ../data/glade/libgweather.xml.in.h:2
msgid "Timezone Menu"
msgstr "Zeitzonenmenü"

#: ../data/glade/libgweather.xml.in.h:3
msgid "Timezone"
msgstr "Zeitzone"

# oder GNOME-Wetter ?
#: ../data/glade/libgweather.xml.in.h:4
msgid "GWeather"
msgstr "GWeather"

#: ../libgweather/gweather-location-entry.c:792
msgid "Loading…"
msgstr "Wird geladen …"

#: ../libgweather/gweather-timezone.c:341
msgid "Greenwich Mean Time"
msgstr "Greenwich Mean Time"

#: ../libgweather/gweather-timezone-menu.c:274
msgctxt "timezone"
msgid "Unknown"
msgstr "Unbekannt"

#: ../libgweather/gweather-weather.c:114
msgid "Variable"
msgstr "Veränderlich"

#: ../libgweather/gweather-weather.c:115
msgid "North"
msgstr "Nord"

#: ../libgweather/gweather-weather.c:115
msgid "North — NorthEast"
msgstr "Nordnordost"

#: ../libgweather/gweather-weather.c:115
msgid "Northeast"
msgstr "Nordosten"

#: ../libgweather/gweather-weather.c:115
msgid "East — NorthEast"
msgstr "Ostnordost"

#: ../libgweather/gweather-weather.c:116
msgid "East"
msgstr "Ost"

#: ../libgweather/gweather-weather.c:116
msgid "East — Southeast"
msgstr "Ostsüdost"

#: ../libgweather/gweather-weather.c:116
msgid "Southeast"
msgstr "Südost"

#: ../libgweather/gweather-weather.c:116
msgid "South — Southeast"
msgstr "Südsüdost"

#: ../libgweather/gweather-weather.c:117
msgid "South"
msgstr "Süd"

#: ../libgweather/gweather-weather.c:117
msgid "South — Southwest"
msgstr "Südsüdwest"

#: ../libgweather/gweather-weather.c:117
msgid "Southwest"
msgstr "Südwest"

#: ../libgweather/gweather-weather.c:117
msgid "West — Southwest"
msgstr "Westsüdwest"

#: ../libgweather/gweather-weather.c:118
msgid "West"
msgstr "West"

#: ../libgweather/gweather-weather.c:118
msgid "West — Northwest"
msgstr "Westnordwest"

#: ../libgweather/gweather-weather.c:118
msgid "Northwest"
msgstr "Nordwest"

#: ../libgweather/gweather-weather.c:118
msgid "North — Northwest"
msgstr "Nordnordwest"

#: ../libgweather/gweather-weather.c:125
msgctxt "wind direction"
msgid "Invalid"
msgstr "Ungültig"

#: ../libgweather/gweather-weather.c:131
msgid "Clear Sky"
msgstr "Klar"

#: ../libgweather/gweather-weather.c:132
msgid "Broken clouds"
msgstr "Teils wolkig"

#: ../libgweather/gweather-weather.c:133
msgid "Scattered clouds"
msgstr "Leicht bewölkt"

#: ../libgweather/gweather-weather.c:134
msgid "Few clouds"
msgstr "Vereinzelt bewölkt"

#: ../libgweather/gweather-weather.c:135
msgid "Overcast"
msgstr "Bedeckt"

#: ../libgweather/gweather-weather.c:142 ../libgweather/gweather-weather.c:214
msgctxt "sky conditions"
msgid "Invalid"
msgstr "Ungültig"

#. TRANSLATOR: If you want to know what "blowing" "shallow" "partial"
#. * etc means, you can go to http://www.weather.com/glossary/ and
#. * http://www.crh.noaa.gov/arx/wx.tbl.php
#. NONE
#: ../libgweather/gweather-weather.c:174 ../libgweather/gweather-weather.c:176
msgid "Thunderstorm"
msgstr "Gewitter"

#. DRIZZLE
#: ../libgweather/gweather-weather.c:175
msgid "Drizzle"
msgstr "Nieselregen"

#: ../libgweather/gweather-weather.c:175
msgid "Light drizzle"
msgstr "Leichter Nieselregen"

#: ../libgweather/gweather-weather.c:175
msgid "Moderate drizzle"
msgstr "Mäßiger Nieselregen"

#: ../libgweather/gweather-weather.c:175
msgid "Heavy drizzle"
msgstr "Starker Nieselregen"

#: ../libgweather/gweather-weather.c:175
msgid "Freezing drizzle"
msgstr "Gefrierender Nieselregen"

#. RAIN
#: ../libgweather/gweather-weather.c:176
msgid "Rain"
msgstr "Regen"

#: ../libgweather/gweather-weather.c:176
msgid "Light rain"
msgstr "Leichter Regen"

#: ../libgweather/gweather-weather.c:176
msgid "Moderate rain"
msgstr "Mäßiger Regen"

#: ../libgweather/gweather-weather.c:176
msgid "Heavy rain"
msgstr "Starker Regen"

#: ../libgweather/gweather-weather.c:176
msgid "Rain showers"
msgstr "Regenschauer"

#: ../libgweather/gweather-weather.c:176
msgid "Freezing rain"
msgstr "Gefrierender Regen"

#. SNOW
#: ../libgweather/gweather-weather.c:177
msgid "Snow"
msgstr "Schneefall"

#: ../libgweather/gweather-weather.c:177
msgid "Light snow"
msgstr "Leichter Schneefall"

#: ../libgweather/gweather-weather.c:177
msgid "Moderate snow"
msgstr "Mäßiger Schneefall"

#: ../libgweather/gweather-weather.c:177
msgid "Heavy snow"
msgstr "Schwerer Schneefall"

#: ../libgweather/gweather-weather.c:177
msgid "Snowstorm"
msgstr "Schneesturm"

#: ../libgweather/gweather-weather.c:177
msgid "Blowing snowfall"
msgstr "Schneetreiben"

#: ../libgweather/gweather-weather.c:177
msgid "Snow showers"
msgstr "Schneeschauer"

#: ../libgweather/gweather-weather.c:177
msgid "Drifting snow"
msgstr "Schneeverwehungen"

#. SNOW_GRAINS
#: ../libgweather/gweather-weather.c:178
msgid "Snow grains"
msgstr "Schneegriesel"

#: ../libgweather/gweather-weather.c:178
msgid "Light snow grains"
msgstr "Leichter Schneegriesel"

#: ../libgweather/gweather-weather.c:178
msgid "Moderate snow grains"
msgstr "Mäßiger Schneegriesel"

#: ../libgweather/gweather-weather.c:178
msgid "Heavy snow grains"
msgstr "Starker Schneegriesel"

#. ICE_CRYSTALS
#: ../libgweather/gweather-weather.c:179
msgid "Ice crystals"
msgstr "Eiskristalle"

# http://de.wikipedia.org/wiki/Eiskorn
#. ICE_PELLETS
#: ../libgweather/gweather-weather.c:180
msgid "Sleet"
msgstr "Eisregen"

#: ../libgweather/gweather-weather.c:180
msgid "Little sleet"
msgstr "Leichter Eisregen"

#: ../libgweather/gweather-weather.c:180
msgid "Moderate sleet"
msgstr "Mäßiger Eisregen"

#: ../libgweather/gweather-weather.c:180
msgid "Heavy sleet"
msgstr "Schwerer Eisregen"

#: ../libgweather/gweather-weather.c:180
msgid "Sleet storm"
msgstr "Eisregensturm"

#: ../libgweather/gweather-weather.c:180
msgid "Showers of sleet"
msgstr "Eisregenschauer"

#. HAIL
#: ../libgweather/gweather-weather.c:181
msgid "Hail"
msgstr "Hagel"

#: ../libgweather/gweather-weather.c:181
msgid "Hailstorm"
msgstr "Hagelsturm"

#: ../libgweather/gweather-weather.c:181
msgid "Hail showers"
msgstr "Hagelschauer"

#. SMALL_HAIL
#: ../libgweather/gweather-weather.c:182
msgid "Small hail"
msgstr "Leichter Hagel"

#: ../libgweather/gweather-weather.c:182
msgid "Small hailstorm"
msgstr "Leichter Hagelsturm"

#: ../libgweather/gweather-weather.c:182
msgid "Showers of small hail"
msgstr "Hagelschauer"

#. PRECIPITATION
#: ../libgweather/gweather-weather.c:183
msgid "Unknown precipitation"
msgstr "Unbekannte Niederschläge"

#. MIST
#: ../libgweather/gweather-weather.c:184
msgid "Mist"
msgstr "Nebel"

#. FOG
#: ../libgweather/gweather-weather.c:185
msgid "Fog"
msgstr "Dichter Nebel"

#: ../libgweather/gweather-weather.c:185
msgid "Fog in the vicinity"
msgstr "Dichter Nebel in der Umgebung"

#: ../libgweather/gweather-weather.c:185
msgid "Shallow fog"
msgstr "Dichter Bodennebel"

#: ../libgweather/gweather-weather.c:185
msgid "Patches of fog"
msgstr "Vereinzelt dichter Nebel"

#: ../libgweather/gweather-weather.c:185
msgid "Partial fog"
msgstr "Teils dichter Nebel"

#: ../libgweather/gweather-weather.c:185
msgid "Freezing fog"
msgstr "Gefrierender Nebel"

#. SMOKE
#: ../libgweather/gweather-weather.c:186
msgid "Smoke"
msgstr "Rauch"

#. VOLCANIC_ASH
#: ../libgweather/gweather-weather.c:187
msgid "Volcanic ash"
msgstr "Vulkanasche"

#. SAND
#: ../libgweather/gweather-weather.c:188
msgid "Sand"
msgstr "Sand"

#: ../libgweather/gweather-weather.c:188
msgid "Blowing sand"
msgstr "Sandtreiben"

#: ../libgweather/gweather-weather.c:188
msgid "Drifting sand"
msgstr "Sandverwehungen"

#. HAZE
#: ../libgweather/gweather-weather.c:189
msgid "Haze"
msgstr "Dunst"

#. SPRAY
#: ../libgweather/gweather-weather.c:190
msgid "Blowing sprays"
msgstr "Aufgewirbeltes Seewasser"

#. DUST
#: ../libgweather/gweather-weather.c:191
msgid "Dust"
msgstr "Staub"

#: ../libgweather/gweather-weather.c:191
msgid "Blowing dust"
msgstr "Staubtreiben"

#: ../libgweather/gweather-weather.c:191
msgid "Drifting dust"
msgstr "Staubverwehungen"

#. SQUALL
#: ../libgweather/gweather-weather.c:192
msgid "Squall"
msgstr "Windböen"

#. SANDSTORM
#: ../libgweather/gweather-weather.c:193
msgid "Sandstorm"
msgstr "Sandsturm"

#: ../libgweather/gweather-weather.c:193
msgid "Sandstorm in the vicinity"
msgstr "Sandsturm in der Umgebung"

#: ../libgweather/gweather-weather.c:193
msgid "Heavy sandstorm"
msgstr "Schwerer Sandsturm"

#. DUSTSTORM
#: ../libgweather/gweather-weather.c:194
msgid "Duststorm"
msgstr "Staubsturm"

#: ../libgweather/gweather-weather.c:194
msgid "Duststorm in the vicinity"
msgstr "Staubsturm in der Umgebung"

#: ../libgweather/gweather-weather.c:194
msgid "Heavy duststorm"
msgstr "Heftiger Staubsturm"

#. FUNNEL_CLOUD
#: ../libgweather/gweather-weather.c:195
msgid "Funnel cloud"
msgstr "Gewitterwolken"

#. TORNADO
#: ../libgweather/gweather-weather.c:196
msgid "Tornado"
msgstr "Tornado"

#. DUST_WHIRLS
#: ../libgweather/gweather-weather.c:197
msgid "Dust whirls"
msgstr "Staubteufel"

#: ../libgweather/gweather-weather.c:197
msgid "Dust whirls in the vicinity"
msgstr "Staubteufel in der Umgebung"

#: ../libgweather/gweather-weather.c:709
msgid "%a, %b %d / %H∶%M"
msgstr "%a, %e. %b / %H:%M"

#: ../libgweather/gweather-weather.c:715
msgid "Unknown observation time"
msgstr "Unbekannte Beobachtungszeit"

#: ../libgweather/gweather-weather.c:727
msgctxt "sky conditions"
msgid "Unknown"
msgstr "Unbekannt"

# Nicht übersetzen: default. Siehe Kommentar für Übersetzer. Es geht nur um mm oder inch.
#. Translate to the default units to use for presenting
#. * lengths to the user. Translate to default:inch if you
#. * want inches, otherwise translate to default:mm.
#. * Do *not* translate it to "predefinito:mm", if it
#. * it isn't default:mm or default:inch it will not work
#.
#: ../libgweather/gweather-weather.c:749
msgid "default:mm"
msgstr "default:mm"

#. TRANSLATOR: This is the temperature in degrees Fahrenheit (\302\260 is U+00B0 DEGREE SIGN)
#: ../libgweather/gweather-weather.c:801
#, c-format
msgid "%.1f °F"
msgstr "%.1f °F"

#. TRANSLATOR: This is the temperature in degrees Fahrenheit (\302\260 is U+00B0 DEGREE SIGN)
#: ../libgweather/gweather-weather.c:804
#, c-format
msgid "%d °F"
msgstr "%d °F"

#. TRANSLATOR: This is the temperature in degrees Celsius (\302\260 is U+00B0 DEGREE SIGN)
#: ../libgweather/gweather-weather.c:810
#, c-format
msgid "%.1f °C"
msgstr "%.1f °C"

#. TRANSLATOR: This is the temperature in degrees Celsius (\302\260 is U+00B0 DEGREE SIGN)
#: ../libgweather/gweather-weather.c:813
#, c-format
msgid "%d °C"
msgstr "%d °C"

#. TRANSLATOR: This is the temperature in kelvin
#: ../libgweather/gweather-weather.c:819
#, c-format
msgid "%.1f K"
msgstr "%.1f K"

#. TRANSLATOR: This is the temperature in kelvin
#: ../libgweather/gweather-weather.c:822
#, c-format
msgid "%d K"
msgstr "%d K"

#: ../libgweather/gweather-weather.c:845 ../libgweather/gweather-weather.c:861
#: ../libgweather/gweather-weather.c:877 ../libgweather/gweather-weather.c:939
msgctxt "temperature"
msgid "Unknown"
msgstr "Unbekannt"

#: ../libgweather/gweather-weather.c:899
msgctxt "dew"
msgid "Unknown"
msgstr "Unbekannt"

#: ../libgweather/gweather-weather.c:919
msgctxt "humidity"
msgid "Unknown"
msgstr "Unbekannt"

#. TRANSLATOR: This is the humidity in percent
#: ../libgweather/gweather-weather.c:922
#, c-format
msgid "%.f%%"
msgstr "%.f%%"

#. TRANSLATOR: This is the wind speed in knots
#: ../libgweather/gweather-weather.c:968
#, c-format
msgid "%0.1f knots"
msgstr "%0.1f Knoten"

#. TRANSLATOR: This is the wind speed in miles per hour
#: ../libgweather/gweather-weather.c:971
#, c-format
msgid "%.1f mph"
msgstr "%.1f mph"

#. TRANSLATOR: This is the wind speed in kilometers per hour
#: ../libgweather/gweather-weather.c:974
#, c-format
msgid "%.1f km/h"
msgstr "%.1f km/h"

#. TRANSLATOR: This is the wind speed in meters per second
#: ../libgweather/gweather-weather.c:977
#, c-format
msgid "%.1f m/s"
msgstr "%.1f m/s"

#. TRANSLATOR: This is the wind speed as a Beaufort force factor
#. * (commonly used in nautical wind estimation).
#.
#: ../libgweather/gweather-weather.c:982
#, c-format
msgid "Beaufort force %.1f"
msgstr "Beaufort-Stärke %.1f"

#: ../libgweather/gweather-weather.c:1003
msgctxt "wind speed"
msgid "Unknown"
msgstr "Unbekannt"

#: ../libgweather/gweather-weather.c:1005
msgid "Calm"
msgstr "Ruhig"

#. TRANSLATOR: This is 'wind direction' / 'wind speed'
#: ../libgweather/gweather-weather.c:1013
#, c-format
msgid "%s / %s"
msgstr "%s / %s"

#: ../libgweather/gweather-weather.c:1049
msgctxt "pressure"
msgid "Unknown"
msgstr "Unbekannt"

#. TRANSLATOR: This is pressure in inches of mercury
#: ../libgweather/gweather-weather.c:1055
#, c-format
msgid "%.2f inHg"
msgstr "%.2f inHg"

#. TRANSLATOR: This is pressure in millimeters of mercury
#: ../libgweather/gweather-weather.c:1058
#, c-format
msgid "%.1f mmHg"
msgstr "%.1f mmHg"

#. TRANSLATOR: This is pressure in kiloPascals
#: ../libgweather/gweather-weather.c:1061
#, c-format
msgid "%.2f kPa"
msgstr "%.2f kPa"

#. TRANSLATOR: This is pressure in hectoPascals
#: ../libgweather/gweather-weather.c:1064
#, c-format
msgid "%.2f hPa"
msgstr "%.2f hPa"

#. TRANSLATOR: This is pressure in millibars
#: ../libgweather/gweather-weather.c:1067
#, c-format
msgid "%.2f mb"
msgstr "%.2f mb"

#. TRANSLATOR: This is pressure in atmospheres
#: ../libgweather/gweather-weather.c:1070
#, c-format
msgid "%.3f atm"
msgstr "%.3f atm"

#: ../libgweather/gweather-weather.c:1108
msgctxt "visibility"
msgid "Unknown"
msgstr "Unbekannt"

#. TRANSLATOR: This is the visibility in miles
#: ../libgweather/gweather-weather.c:1114
#, c-format
msgid "%.1f miles"
msgstr "%.1f Meilen"

#. TRANSLATOR: This is the visibility in kilometers
#: ../libgweather/gweather-weather.c:1117
#, c-format
msgid "%.1f km"
msgstr "%.1f km"

#. TRANSLATOR: This is the visibility in meters
#: ../libgweather/gweather-weather.c:1120
#, c-format
msgid "%.0fm"
msgstr "%.0f m"

#: ../libgweather/gweather-weather.c:1148
#: ../libgweather/gweather-weather.c:1173
msgid "%H∶%M"
msgstr "%H∶%M"

#: ../libgweather/gweather-weather.c:1267
msgid "Retrieval failed"
msgstr "Abrufen fehlgeschlagen"

#: ../libgweather/weather-metar.c:573
#, c-format
msgid "Failed to get METAR data: %d %s.\n"
msgstr "METAR-Daten konnten nicht geholt werden: %d %s.\n"

#: ../libgweather/weather-owm.c:383
msgid ""
"Weather data from the <a href=\"http://openweathermap.org\">Open Weather Map "
"project</a>"
msgstr ""
"Wetterdaten vom <a href=\"http://openweathermap.org\">Open Weather Map "
"Projekt</a>"

#. The new (documented but not advertised) API is less strict in the
#. format of the attribution, and just requires a generic CC-BY compatible
#. attribution with a link to their service.
#.
#. That's very nice of them!
#.
#: ../libgweather/weather-yrno.c:507
msgid ""
"Weather data from the <a href=\"http://www.met.no/\">Norwegian "
"Meteorological Institute</a>"
msgstr ""
"Wetterdaten vom <a href=\"http://www.met.no/\">Norwegischen Meteorologischen "
"Institut</a>"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:1
msgid "URL for the radar map"
msgstr "URL zur Radarkarte"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:2
msgid ""
"The custom URL from where to retrieve a radar map, or empty for disabling "
"radar maps."
msgstr ""
"Die Benutzerdefinierte URL, von der die Radarkarte abgerufen werden soll. "
"Leer lassen, um Radarkarten zu deaktivieren."

#: ../schemas/org.gnome.GWeather.gschema.xml.h:3
msgid "Temperature unit"
msgstr "Temperatureinheit"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:4
msgid ""
"The unit of temperature used for showing weather. Valid values are “kelvin”, "
"“centigrade” and “fahrenheit”."
msgstr ""
"Die Temperatureinheit, die zur Anzeige des Wetters verwendet wird. Gültige "
"Werte sind »kelvin« (Kelvin), »centigrade« (Grad Celsius) und "
"»fahrenheit« (Grad Fahrenheit)."

#: ../schemas/org.gnome.GWeather.gschema.xml.h:5
msgid "Distance unit"
msgstr "Entfernungseinheit"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:6
msgid ""
"The unit of distance used for showing weather (for example for visibility or "
"for distance of important events). Valid values are “meters”, “km” and "
"“miles”."
msgstr ""
"Die Entfernungseinheit, die zur Anzeige des Wetters verwendet wird, zum "
"Beispiel für die Fernsicht oder die Entfernung bedeutender Wetterereignisse. "
"Gültige Werte sind »meters« (Meter), »km« (Kilometer) und »miles« (Meilen)."

#: ../schemas/org.gnome.GWeather.gschema.xml.h:7
msgid "Speed unit"
msgstr "Geschwindigkeitseinheit"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:8
msgid ""
"The unit of speed used for showing weather (for example for wind speed). "
"Valid values are “ms” (meters per second), “kph” (kilometers per hour), "
"“mph” (miles per hour), “knots” and “bft” (Beaufort scale)."
msgstr ""
"Die Geschwindigkeitseinheit, die zur Anzeige des Wetters verwendet wird, zum "
"Beispiel für die Windgeschwindigkeit. Gültige Werte sind »ms« (Meter pro "
"Sekunde), »kph« (Kilometer pro Stunde), »mph« (Meilen pro Stunde), "
"»knots« (Knoten) und »bft« (Windstärke nach der Beaufort-Skala)."

#: ../schemas/org.gnome.GWeather.gschema.xml.h:9
msgid "Pressure unit"
msgstr "Druckeinheit"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:10
msgid ""
"The unit of pressure used for showing weather. Valid values are "
"“kpa” (kilopascal), “hpa” (hectopascal), “mb” (millibar, mathematically "
"equivalent to 1 hPa but shown differently), “mm-hg” (millimiters of "
"mercury), “inch-hg” (inches of mercury), “atm” (atmospheres)."
msgstr ""
"Die Druckeinheit, die zur Anzeige des Wetters verwendet wird. Gültige Werte "
"sind »kpa« (Kilopascal), »hpa« (Hektopascal), »mb« (Millibar, Wert identisch "
"zu Hektopascal), »mm-hg« (Millimeter Quecksilbersäule), »inch-hg« (Zoll "
"Quecksilbersäule) und »atm« (Atmosphären)."

#: ../schemas/org.gnome.GWeather.gschema.xml.h:11
msgid "Default location"
msgstr "Vorgegebener Standort"

#: ../schemas/org.gnome.GWeather.gschema.xml.h:12
msgid ""
"The default location for the weather applet. The first field is the name "
"that will be shown. If empty, it will be taken from the locations database. "
"The second field is the METAR code for the default weather station. It must "
"not be empty and must correspond to a &lt;code&gt; tag in the Locations.xml "
"file. The third field is a tuple of (latitude, longitude), to override the "
"value taken from the database. This is only used for sunrise and moon phase "
"calculations, not for weather forecast."
msgstr ""
"Der vorgegebene Ort für das Wetter-Applet. Das erste Feld enthält den "
"angezeigten Namen. Falls leer, wird der Name aus der Orte-Datenbank "
"entnommen. Das zweite Feld enthält den METAR-Code der vorgegebenen "
"Wetterstation. Es darf nicht leer sein und muss dem »code«-Tag in der Datei "
"Locations.xml entsprechen. Das dritte Feld enthält ein Tupel aus "
"(Breitengrad, Längengrad), welches Vorrang vor den Werten aus der Datenbank "
"hat. Dies gilt nur für die Berechnung von Sonnenaufgängen und Mondphasen und "
"nicht für die Wettervorhersage."

#~ msgid "Ice pellets"
#~ msgstr "Graupel"

#~ msgid "Few ice pellets"
#~ msgstr "Wenig Graupel"

#~ msgid "Moderate ice pellets"
#~ msgstr "Mäßiger Graupel"

#~ msgid "Heavy ice pellets"
#~ msgstr "Schwerer Graupel"

#~ msgctxt "temperature unit"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt "speed unit"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt "pressure unit"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt "visibility unit"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "'fahrenheit'"
#~ msgstr "'centigrade'"

#~ msgid "'miles'"
#~ msgstr "'km'"

#~ msgid "'knots'"
#~ msgstr "'ms'"

#~ msgid "'inch-hg'"
#~ msgstr "'hpa'"

#~ msgid "%H:%M"
#~ msgstr "%H:%M"

#~ msgid "WeatherInfo missing location"
#~ msgstr "Wetterinfo konnte Standort nicht finden"

#~ msgid "DEFAULT_LOCATION"
#~ msgstr "Berlin"

#~ msgid "DEFAULT_CODE"
#~ msgstr "EDDM"

#~ msgid "DEFAULT_ZONE"
#~ msgstr "DEFAULT_ZONE"

#~ msgid "DEFAULT_RADAR"
#~ msgstr "DEFAULT_RADAR"

#~ msgid "DEFAULT_COORDINATES"
#~ msgstr "48-21N 011-47E"

#~ msgid "K"
#~ msgstr "K"

#~ msgid "C"
#~ msgstr "C"

#~ msgid "F"
#~ msgstr "F"

#~ msgid "m/s"
#~ msgstr "m/s"

#~ msgid "km/h"
#~ msgstr "km/h"

#~ msgid "mph"
#~ msgstr "mph"

#~ msgid "Beaufort scale"
#~ msgstr "Beaufort-Skala"

#~ msgid "kPa"
#~ msgstr "kPa"

#~ msgid "hPa"
#~ msgstr "hPa"

#~ msgid "mb"
#~ msgstr "mbar"

#~ msgid "mmHg"
#~ msgstr "mmHg"

#~ msgid "inHg"
#~ msgstr "inHg"

#~ msgid "atm"
#~ msgstr "atm"

#~ msgid "m"
#~ msgstr "m"

#~ msgid "km"
#~ msgstr "km"

#~ msgid "mi"
#~ msgstr "mi"

#~ msgid "DEFAULT_TEMP_UNIT"
#~ msgstr "C"

#~ msgid "DEFAULT_SPEED_UNIT"
#~ msgstr "m/s"

#~ msgid "DEFAULT_PRESSURE_UNIT"
#~ msgstr "hPa"

#~ msgid "DEFAULT_DISTANCE_UNIT"
#~ msgstr "m"

#~ msgid ""
#~ "A three-digit-long code for retrieving radar maps from weather.com, found "
#~ "from http://git.gnome.org/cgit/libgweather/plain/data/Locations.xml.in"
#~ msgstr ""
#~ "Ein dreistelliger Code zum Abfragen der Radarkarten von weather.com, "
#~ "siehe http://git.gnome.org/cgit/libgweather/plain/data/Locations.xml.in"

#~ msgid ""
#~ "A unique zone for the city, as found from http://git.gnome.org/cgit/"
#~ "libgweather/plain/data/Locations.xml.in"
#~ msgstr ""
#~ "Eine eindeutige Zone für die Stadt, siehe http://git.gnome.org/cgit/"
#~ "libgweather/plain/data/Locations.xml.in"

#~ msgid ""
#~ "Determines whether the applet automatically updates its weather "
#~ "statistics or not."
#~ msgstr ""
#~ "Legt fest, ob das Applet automatisch seine Wetterstatistiken aktualisiert."

#~ msgid "Display radar map"
#~ msgstr "Radarkarte anzeigen"

#~ msgid "Fetch a radar map on each update."
#~ msgstr "Bei jedem Aktualisierungsvorgang eine Radarkarte abrufen."

#~ msgid ""
#~ "If true, then retrieve a radar map from a location specified by the "
#~ "\"radar\" key."
#~ msgstr ""
#~ "Falls dieser Schlüssel wahr ist, wird eine Radarkarte von einem Ort "
#~ "abgerufen, der vom Schlüssel »radar« angegeben wird."

#~ msgid ""
#~ "Latitude and longitude of your location expressed in DD-MM-SS[NS] DD-MM-"
#~ "SS[EW]."
#~ msgstr ""
#~ "Breiten- und Längengrad Ihres aktuellen Standorts, angegeben in GG-MM-"
#~ "SS[NS] GG-MM-SS[OW], also Grad-Minuten-Sekunden[Nord-Süd] bzw. [Ost-West]."

#~ msgid "Nearby city"
#~ msgstr "Nächstgelegene Stadt"

#~ msgid ""
#~ "Nearby major zone, such as a capital city, as found from http://git.gnome."
#~ "org/cgit/libgweather/plain/data/Locations.xml.in"
#~ msgstr ""
#~ "Nächstgelegene Zone, z.B. einer Hauptstadt, siehe http://git.gnome.org/"
#~ "cgit/libgweather/plain/data/Locations.xml.in"

#~ msgid "Not used anymore"
#~ msgstr "Nicht mehr verwendet"

#~ msgid "Radar location"
#~ msgstr "Radarstandort"

#~ msgid "The city that gweather displays information for."
#~ msgstr "Die Stadt, für die gweather die Informationen darstellt."

#~ msgid "The interval, in seconds, between automatic updates."
#~ msgstr "Das Intervall zwischen automatischen Aktualisierungen in Sekunden."

#~ msgid "The unit to use for pressure."
#~ msgstr "Die für den Druck zu verwendente Einheit."

#~ msgid "The unit to use for temperature."
#~ msgstr "Die für die Temperatur zu verwendente Einheit."

#~ msgid "The unit to use for visibility."
#~ msgstr "Die für die Sichtweite zu verwendente Einheit."

#~ msgid "The unit to use for wind speed."
#~ msgstr "Die für die Windgeschwindigkeit zu verwendente Einheit."

#~ msgid "Update interval"
#~ msgstr "Aktualisierungsintervall"

#~ msgid "Update the data automatically"
#~ msgstr "Die Daten automatisch aktualisieren"

#~ msgid "Use custom url for the radar map"
#~ msgstr "Benutzerdefinierte URL für die Radarkarte verwenden"

#~ msgid "Use metric units"
#~ msgstr "Metrische Einheiten verwenden"

#~ msgid "Use metric units instead of english units."
#~ msgstr "Metrische statt englische Einheiten verwenden"

#~ msgid "Weather for a city"
#~ msgstr "Wetter für eine Stadt"

#~ msgid "Weather location information"
#~ msgstr "Informationen zum Wetterstandort."

#~ msgid "Weather location information."
#~ msgstr "Informationen zum Wetterstandort."

#~ msgid "%.1f ℉"
#~ msgstr "%.1f ℉"

#~ msgid "%d ℉"
#~ msgstr "%d ℉"

#~ msgid "%.1f ℃"
#~ msgstr "%.1f ℃"

#~ msgid "%d ℃"
#~ msgstr "%d ℃"
